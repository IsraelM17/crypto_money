import React, { useEffect, useState } from "react";
import styled from "@emotion/styled";
import useMoneda from "../hooks/useMoneda";
import useCryptoMoneda from "../hooks/useCryptoMoneda";
import axios from 'axios';

const Boton = styled.input`
    margin-top: 20px;
    font-weight: bold;
    font-size: 20px;
    padding: 10px;
    background-color: #66a2fe;
    border: none;
    width: 100%;
    border-radius: 10px;
    color: #fff;
    transition: background-color .3s ease;
    
    &:hover{
        background-color: #326ac0;
        cursor: pointer;
    }
`;

const Formulario = () => {

    //state listado crypto
    const [ cryptos, guardaCryptos ] = useState([]);

    const MONEDAS = [
        {codigo: 'USD', nombre: 'Dolar Estadounidense'},
        {codigo: 'MXN', nombre: 'Peso Mexicano'},
        {codigo: 'EUR', nombre: 'Euro'},
        {codigo: 'GBP', nombre: 'Libra Esterlina'}
    ];

    //Utilizar useMoneda
    const [ moneda, SelectMonedas ] = useMoneda('Elige tu moneda', '', MONEDAS);

    //Utilizar useCriptoMoneda
    const [ criptomoneda, SelectCripto ] = useCryptoMoneda('Elige tu crypto moneda', '', cryptos);

    //Ejecutar llamado al Api
    useEffect(()=>{
        const consultarAPI = async () => {
            const url = `https://min-api.cryptocompare.com/data/top/mktcapfull?limit=10&tsym=MXN`;

            const resultado = await axios.get(url);

            guardaCryptos(resultado.data.Data);
        };
        consultarAPI();
    }, []);

    return (
        <form>
            <SelectMonedas/>
            <SelectCripto/>
            <Boton
                type = "submit"
                value= "Calcular"
            />
        </form>
    );

};

export default Formulario;